--------------------------------------------------------------------------
--[[ VolcanoActivity class definition ]]
--------------------------------------------------------------------------

return Class(function(self, inst)

--------------------------------------------------------------------------
--[[ Dependencies ]]
--------------------------------------------------------------------------

local math = math
local SCHEDULE_DEFS = require("prefabs/volcanoschedule_defs")

--------------------------------------------------------------------------
--[[ Constants ]]
--------------------------------------------------------------------------

local SPAWN_PROTECTION_TIME = TUNING.VOLCANO_SPAWN_PROTECTION_TIME

local DRAGOON_CHANCE = TUNING.VOLCANO_DRAGOONEGG_CHANCE
local FIRERAIN_RADIUS = TUNING.VOLCANO_FIRERAIN_RADIUS
local FIRERAIN_INTENSITY = TUNING.VOLCANO_FIRERAIN_INTENSITY

local STAFF_FIRERAIN_RADIUS
local STAFF_FIRERAIN_DURATION
local STAFF_FIRERAIN_SPAWN_PER_SEC

local STAFF_ASHRAIN_DURATION = TUNING.VOLCANOSTAFF_ASH_TIMER

--------------------------------------------------------------------------
--[[ Member variables ]]
--------------------------------------------------------------------------

--Public
self.inst = inst

--Private
local _world = TheWorld
local _ismastersim = _world.ismastersim
local _activatedplayer = nil

--Dedicated server does not need to spawn the local fx
local _hasfx = not TheNet:IsDedicated()
local _ashrainfx = _hasfx and SpawnPrefab("ashrain") or nil

local _isVolcanoClimate = false
local _isdryseason = false

--Master simulation
local _warntask
local _firerain_spawn_rate
local _firerain_spawn_per_sec
local _firerain_duration = 0
local _firerain_delay = 0
local _stafffirerain = {}

--Network
local _eruption = net_bool(inst.GUID, "volcanoactivity._eruption", "eruptiondirty")
local _eruption_timer = net_float(inst.GUID, "volcanoactivity._eruption_timer")
local _stafferuption = net_bool(inst.GUID, "volcanoactivity._stafferuption", "stafferuptiondirty")
local _staffashrain_timer = net_float(inst.GUID, "volcanoactivity._stafferuption_timer")
local _firerain = net_bool(inst.GUID, "volcanoactivity._firerain", "fireraindirty")

local _ashrain_duration = net_float(inst.GUID, "volcanoactivity._ashrain_duration")
local _smokecloud_duration = net_float(inst.GUID, "volcanoactivity._smokecloud_duration")

local _ashrain_delay = net_float(inst.GUID, "volcanoactivity._ashrain_delay")
local _smokecloud_delay = net_float(inst.GUID, "volcanoactivity._smokecloud_delay")

local _warnquakesound = net_bool(inst.GUID, "volcanoactivity._warnquakesound", "volcano_warnquakesounddirty")
local _eruptquakesound = net_bool(inst.GUID, "volcanoactivity._eruptquakesound", "volcano_eruptquakesounddirty")

--------------------------------------------------------------------------
--[[ Private member functions ]]
--------------------------------------------------------------------------

local function SetWithPeriodicSync(netvar, val, period, ismastersim)
    if netvar:value() ~= val then
        local trunc = val > netvar:value() and "floor" or "ceil"
        local prevperiod = math[trunc](netvar:value() / period)
        local nextperiod = math[trunc](val / period)

        if prevperiod == nextperiod then
            --Client and server update independently within current period
            netvar:set_local(val)
        elseif ismastersim then
            --Server sync to client when period changes
            netvar:set(val)
        else
            --Client must wait at end of period for a server sync
            netvar:set_local(nextperiod * period)
        end
    elseif ismastersim then
        --Force sync when value stops changing
        netvar:set(val)
    end
end

local function CalculateEruptionDuration()
    local eruption_duration = 0
    eruption_duration = math.max(eruption_duration, _firerain_delay:value() + _firerain_duration:value())
    eruption_duration = math.max(eruption_duration, _ashrain_delay:value() + _ashrain_duration:value())
    eruption_duration = math.max(eruption_duration, _smokecloud_delay:value() + _smokecloud_duration:value())
    return eruption_duration
end

local function CalculateEruptionProgress()
    if _eruption:value() then
        return _eruption_timer:value() / CalculateEruptionDuration()
    end
    return 0.0
end

local function CalculateFireRainProgress()
    if _firerain_duration:value() ~= 0 and _eruption_timer:value() >= _firerain_delay:value() then
        return math.min((_eruption_timer:value() - _firerain_delay:value()) / _firerain_duration:value(), 1.0)
    end
    return 0.0
end

local function GetWorldAshRainRate()
    if _isVolcanoClimate then
        if _isdryseason then
            return 6.0
        end
        return 1.0
    end
    return 0.0
end

local function CalculateAshRainRate()
    local rate = 0.0
    if _staffashrain_duration:value() ~= 0 then
        rate = rate + math.min(_staffashrain_timer:value() / _staffashrain_duration:value(), 1.0)
    end
    if _ashrain_duration:value() ~= 0 and _eruption_timer:value() >= _ashrain_delay:value() then
        rate = rate + math.min((_eruption_timer:value() - _ashrain_delay:value()) / _ashrain_duration:value(), 1.0)
    end
    return rate
end

local function CalculateSmokeCloudDensity()
    if _smokecloud_duration:value() ~= 0 and _eruption_timer:value() >= _smokecloud_delay:value() then
        return math.min((_eruption_timer:value() - _smokecloud_delay:value()) / _smokecloud_duration:value(), 1.0)
    end
    return 0.0
end

local StartFireRain = _ismastersim and function(firerain_duration, firerain_delay, firerain_spawn_per_sec)
    if _firerain_duration:value() == 0 then
        print("Firerain Start", firerain_duration, firerain_spawn_per_sec)

        _firerain_spawn_rate = 0
        _firerain_spawn_per_sec = firerain_spawn_per_sec or 0
        _firerain_duration = firerain_duration or 0
        _firerain_delay = firerain_delay or 0
    end
end or nil

local StopFireRain = _ismastersim and function()
    if _firerain_duration:value() ~= 0 then
        print("Firerain stop")

        _firerain_spawn_rate = 0
        _firerain_spawn_per_sec = 0
        _firerain_duration = 0
        _firerain_delay = 0
    end
end or nil

local StartAshRain = _ismastersim and function(ashrain_duration, ashrain_delay)
    if _ashrain_duration:value() == 0 then
        _ashrain_delay:set(ashrain_delay or 0)
        _ashrain_duration:set(ashrain_duration or 0)
    end
end or nil

local StopAshRain = _ismastersim and function()
    if _ashrain_duration:value() ~= 0 then
        print("Ashrain stop")
        _ashrain_duration:set(0)
        _ashrain_delay:set(0)
    end
end or nil

local StartSmokeCloud = _ismastersim and function(smokecloud_duration, smokecloud_delay)
    if _smokecloud_duration:value() == 0 then
        _smokecloud_delay:set(smokecloud_delay or 0)
        _smokecloud_duration:set(smokecloud_duration or 0)
    end
end or nil

local StopSmokeCloud = _ismastersim and function()
    if _smokecloud_duration:value() ~= 0 then
        _smokecloud_duration:set(0)
        _smokecloud_delay:set(0)
    end
end or nil

local StopWarnSound = _ismastersim and function()
    if _warntask ~= nil then
        _warntask:Cancel()
        _warntask = nil
        _warnquakesound:set(false)
    end
end or nil

local StartWarnSound = _ismastersim and function(duration)
    print("start warn", _warntask)
    if _warntask == nil then
        _warnquakesound:set(true)
        _warntask = _world:DoTaskInTime(duration or 0, StopWarnSound)
    end
end or nil

local StopEruptSound = _ismastersim and function()
    _eruptquakesound:set(false)
end or nil

local StartEruptSound = _ismastersim and function(duration)
    _eruptquakesound:set(true)
end or nil

local _DoWarningSpeech = _ismastersim and function(player)
    player.components.talker:Say(GetString(player, "ANNOUNCE_QUAKE"))
end or nil

local _DoEruptionSpeech = _ismastersim and function(player)
    player.components.talker:Say(GetString(player, "ANNOUNCE_VOLCANO_ERUPT"))
end or nil

local DoWarnQuake = _ismastersim and function(duration, speed, scale)
    for _, player in pairs(AllPlayers) do
        if IsInIAClimate(player) then
            player:ShakeCamera(CAMERASHAKE.FULL, duration, speed, scale)
            player:DoTaskInTime(math.random() * 2, _DoWarningSpeech) -- Nice looking delay I stole from the quaker cmp -Half
        end
    end
    _world:PushEvent("OnVolcanoWarningQuake")
end or nil

local DoEruptQuake = _ismastersim and function(duration, speed, scale)
    for _, player in pairs(AllPlayers) do
        if IsInIAClimate(player) then
            player:ShakeCamera(CAMERASHAKE.FULL, duration, speed, scale)
            player:DoTaskInTime(math.random() * 2, _DoEruptionSpeech) -- Nice looking delay I stole from the quaker cmp -Half
        end
    end
    _world:PushEvent("OnVolcanoWarningQuake")
end or nil

local DoEruption = _ismastersim and function()
    if not _eruption:value() then
        _eruption_timer:set(0)
        _eruption:set(true)
    end
end

local SpawnFireRain = _ismastersim and function(x, z, egg)
    local firerain
    if egg then
        firerain = SpawnPrefab("dragoonegg_falling")
    else
        firerain = SpawnPrefab("firerain")
    end
    firerain.Transform:SetPosition(x, 0, z)
    firerain:StartStep()
end or nil

local function PushVolcanoActivity()
    local data =
    {
        eruption_progress = CalculateEruptionProgress(),
        firerain_progress = CalculateFireRainProgress(),
        ashrainrate = CalculateAshRainRate() + GetWorldAshRainRate() / 20,
        smokecloud_density = CalculateSmokeCloudDensity(),
    }
    _world:PushEvent("volcanoactivitytick", data)
end

--------------------------------------------------------------------------
--[[ Event Callbacks ]]
--------------------------------------------------------------------------

local function OnClimateChanged()
    if _activatedplayer then
        _isVolcanoClimate = IsInClimate(_activatedplayer, "volcano")
    end
end

local function OnDrySeasonChanged(src, isdry)
    _isdryseason = isdry
end

local function OnPlayerActivated(src, player)
    _activatedplayer = player
    player:ListenForEvent("climatechange", OnClimateChanged)
    if _hasfx then
        _ashrainfx.entity:SetParent(player.entity)
        self:OnPostInit()
    end
end

local function OnPlayerDeactivated(src, player)
    player:RemoveEventCallback("climatechange", OnClimateChanged)
    if _activatedplayer == player then
        _activatedplayer = nil
    end
    if _hasfx then
        _ashrainfx.entity:SetParent(nil)
    end
end

local function OnWarnQuakeSoundDirty()
    print("warnsound", _warnquakesound:value())
    if _warnquakesound:value() then
        if not _world.SoundEmitter:PlayingSound("volcano_earthquake") then
            _world.SoundEmitter:PlaySound("dontstarve/cave/earthquake", "volcano_earthquake")
        end
        _world.SoundEmitter:SetParameter("volcano_earthquake", "intensity", 0.08)
    elseif _world.SoundEmitter:PlayingSound("volcano_earthquake") then
        _world.SoundEmitter:KillSound("volcano_earthquake")
    end
end

local function OnEruptQuakeSoundDirty()
    print("eruptsound", _eruptquakesound:value())
    if _eruptquakesound:value() then
        if not _world.SoundEmitter:PlayingSound("volcano_eruption") then
            _world.SoundEmitter:PlaySound("ia/music/music_volcano_active", "volcano_eruption")
        end
    elseif _world.SoundEmitter:PlayingSound("volcano_eruption") then
        _world.SoundEmitter:KillSound("volcano_eruption")
    end
    _world:PushEvent("mutedynamicmusic", {source = "volcano_eruption", mute = _eruptquakesound:value()})
end

-- local function UpdateSounds(src, player)
--     if player == ThePlayer then
--         if _warnquakesound:value() and not _world.SoundEmitter:PlayingSound("volcano_earthquake") then
--             _world.SoundEmitter:PlaySound("dontstarve/cave/earthquake", "volcano_earthquake")
--             _world.SoundEmitter:SetParameter("volcano_earthquake", "intensity", 0.08)
--         end
--         if _eruptquakesound:value() and not _world.SoundEmitter:PlayingSound("volcano_eruption") then
--             _world.SoundEmitter:PlaySound("ia/music/music_volcano_active", "volcano_eruption")
--         end
--     end
-- end

local WarnQuake = _ismastersim and function(src, size)
    local size_def = SCHEDULE_DEFS.WARN_QUAKE[size or "med"]
    local duration = size_def.duration
    local speed = size_def.speed
    local scale = size_def.scale
    
    StartWarnSound(duration)
    DoWarnQuake(duration, speed, scale)
end or nil

local EruptQuake = _ismastersim and function(src, size)
    local size_def = SCHEDULE_DEFS.ERUPT_QUAKE[size or "med"]
    local duration = size_def.duration
    local speed = size_def.speed
    local scale = size_def.scale

    DoEruptQuake(duration, speed, scale)
end or nil

local StartEruption = _ismastersim and function(src, data)
    if not data then
        print("DoEruption no data!")
        return
    end

    StartFireRain(data.firerain_duration, data.firerain_delay, data.firerain_per_sec)
    StartAshRain(data.ashrain_duration, data.ashrain_delay)
    StartSmokeCloud(data.smokecloud_duration, data.smokecloud_delay)

    EruptQuake(data.quakesize)
    StartWarnSound(data.firerain_duration)
    StartEruptSound()

    DoEruption()
    _world:PushEvent("OnVolcanoEruptionBegin")
end or nil

local StopEruption = _ismastersim and function(src)
    StopSmokeCloud()
    StopAshRain()
    StopFireRain()
    StopWarnSound()
    StopEruptSound()

    _eruption:set(false)
end or nil

local function StartStaffFireRain(pos)
    table.insert(_stafffirerain, {
        pos = pos,
        timer = 0,
        spawn_rate = 0,
    })
end

local function StopStaffFireRain(index)
    table.remove(_stafffirerain, index)
end

local function DoStaffEruption(pos)
    StartStaffFireRain(pos)
    
    if not _eruption:value() then
        StartAshRain(data.ashrain_duration, data.ashrain_delay)

        DoEruption()
    end
end

local OnStartStaffEruption = _ismastersim and function(src, pos)
    for id in pairs(Shard_GetConnectedShards()) do
        SendModRPCToShard(SHARD_MOD_RPC["Island Adventure"]["StartStaffEruption"], id) --Dont sent pos
    end
    DoStaffEruption(pos)
end

--------------------------------------------------------------------------
--[[ Post initialization ]]
--------------------------------------------------------------------------

if _hasfx then function self:OnPostInit()
    if _ashrain_duration:value() ~= 0 then
        _ashrainfx:PostInit()
    end
end end

--------------------------------------------------------------------------
--[[ Deinitialization ]]
--------------------------------------------------------------------------

if _hasfx then function self:OnRemoveEntity()
    if _ashrainfx.entity:IsValid() then
        _ashrainfx:Remove()
    end
end end

--------------------------------------------------------------------------
--[[ Update ]]
--------------------------------------------------------------------------

local UpdateFireRain = _ismastersim and function(dt)
    _firerain_spawn_rate = _firerain_spawn_rate + _firerain_spawn_per_sec * FIRERAIN_INTENSITY * dt
    while _firerain_spawn_rate > 1.0 do
        for _, player in pairs(AllPlayers) do
            if player:GetTimeAlive() > SPAWN_PROTECTION_TIME and not player:HasTag("playerghost") then
                local x, y, z = player.Transform:GetWorldPosition()
                local num_players = #FindPlayersInRange(x, 0, z, FIRERAIN_RADIUS, true)
                if num_players <= 1 or math.random() <= 1/num_players then
                    x = x + FIRERAIN_RADIUS * UnitRand()
                    z = z + FIRERAIN_RADIUS * UnitRand()
                    SpawnFireRain(x, z, math.random() <= DRAGOON_CHANCE)
                end
            end
        end
        _firerain_spawn_rate = _firerain_spawn_rate - 1.0
    end
end or nil

local UpdateStaffFireRain = _ismastersim and function(dt, data)
    data.spawn_rate = data.spawn_rate + STAFF_FIRERAIN_SPAWN_PER_SEC * FIRERAIN_INTENSITY * dt
    while data.spawn_rate > 1.0 do
        if data.pos ~= nil then
            local x, y, z = data.pos:Get()
            x = x + FIRERAIN_RADIUS * UnitRand()
            z = z + FIRERAIN_RADIUS * UnitRand()
            SpawnFireRain(x, z)
        end
        data.spawn_rate = data.spawn_rate - 1.0
    end
end or nil

--[[
    Client updates the eruption timer on its own while server force syncs
    values periodically. Client cannot start, stop, or change eruption duration 
    values on its own, and must wait for server syncs to trigger these events.
--]]
function self:OnUpdate(dt)

    --update eruption
    if _eruption:value() then
        SetWithPeriodicSync(_eruption_timer, _eruption_timer:value() + dt, 100, _ismastersim)
    end

    --update staff ash rain
    if _stafferuption:value() and _staffashrain_timer:value() <= STAFF_ASHRAIN_DURATION then
        SetWithPeriodicSync(_staffashrain_timer, _staffashrain_timer:value() + dt, 100, _ismastersim)
    end

    if _ismastersim then

        --update eruption
        if _eruption:value() and _eruption_timer:value() > CalculateEruptionDuration() then
            StopEruption()
        end

        --update staff eruption
        if _stafferuption:value() and _staffashrain_timer:value() > STAFF_ASHRAIN_DURATION and #_stafffirerain < 1 then
            StopStaffEruption()
        end

        --update fire rain
        if FIRERAIN_INTENSITY > 0.0 and _firerain_duration:value() ~= 0 then
            if not _eruption:value() or (_eruption_timer:value() - _firerain_delay:value()) > _firerain_duration:value() then
                StopFireRain()
            elseif _eruption_timer:value() > _firerain_delay:value() then
                UpdateFireRain(dt)
            end
        end

        --update staff fire rain
        for index=#_stafffirerain, 1, -1 do
            local data = _stafffirerain[index]
            data.timer = data.timer + dt
            if not not _eruption:value() or data.timer > STAFF_FIRERAIN_DURATION then
                StopStaffFireRain(index)
            else
                UpdateStaffFireRain(dt, data)
            end
        end

        --update ash rain
        if _ashrain_duration:value() ~= 0 then
            if not _eruption:value() or (_eruption_timer:value() - _ashrain_delay:value()) > _ashrain_duration:value() then
                StopAshRain()
            end
        end

        --update staff ash rain
        if _stafferuption:value() and _staffashrain_timer:value() <= STAFF_ASHRAIN_DURATION then
            if not _eruption:value() or _staffashrain_timer:value() > _staffashrain_duration:value() then
                -- StopStaffAshRain()
            end
        end

        --update smokecloud
        if _smokecloud_duration:value() ~= 0 then
            if not _eruption:value() or (_eruption_timer:value() - _smokecloud_delay:value()) > _smokecloud_duration:value() then
                StopSmokeCloud()
            end
        end
    end

    if _hasfx then
        --update ash rain
        local worldashrate = GetWorldAshRainRate()
        local ashrainrate = CalculateAshRainRate()
        _ashrainfx.particles_per_tick = 20 * ashrainrate + worldashrate
    end

    PushVolcanoActivity()
end

self.LongUpdate = self.OnUpdate

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------

--Initialize network variables
_eruption:set(false)
_firerain_duration:set(0)
_ashrain_duration:set(0)
_smokecloud_duration:set(0)

_warnquakesound:set(false)
_eruptquakesound:set(false)

--Dedicated server does not need to spawn the local fx
if _hasfx then
    --Initialize ash particles
    _ashrainfx.particles_per_tick = 0
end

--Register network variable sync events
inst:ListenForEvent("eruptiondirty", function() print("ERUPTION CHANGED") _world:PushEvent("eruptionchanged", _eruption:value()) end)
if not TheNet:IsDedicated() then
    inst:ListenForEvent("volcano_warnquakesounddirty", OnWarnQuakeSoundDirty)
    inst:ListenForEvent("volcano_eruptquakesounddirty", OnEruptQuakeSoundDirty)
end

--Register events
inst:WatchWorldState("issummer", OnDrySeasonChanged)
inst:ListenForEvent("playeractivated", OnPlayerActivated, _world)
inst:ListenForEvent("playerdeactivated", OnPlayerDeactivated, _world)

--Register events

if _ismastersim then
    --Initialize master simulation variables
    _warntask = nil

    _firerain_spawn_rate = 0
    _firerain_spawn_per_sec = 0

    --Register master simulation events
    inst:ListenForEvent("ms_warnquake", WarnQuake, _world)
    inst:ListenForEvent("ms_eruptquake", EruptQuake, _world)
    inst:ListenForEvent("ms_starteruption", StartEruption, _world)
    inst:ListenForEvent("ms_stoperuption", StopEruption, _world)
    inst:ListenForEvent("ms_startstafferuption", OnStartStaffEruption, _world)

    -- inst:ListenForEvent("ms_forcefirerain", OnForceFireRain, _world)
    -- inst:ListenForEvent("ms_forceashrain", OnForceAshRain, _world)
    -- inst:ListenForEvent("ms_forcesmokecloud", OnForceSmokeCloud, _world)

    inst:ListenForEvent("shard_startstafferuption", DoStaffEruption, _world)
end

-- if not TheNet:IsDedicated() then
--     inst:ListenForEvent("ms_playerjoined", UpdateSounds, _world)
-- end

inst:StartUpdatingComponent(self)

--------------------------------------------------------------------------
--[[ Save/Load ]]
--------------------------------------------------------------------------

if _ismastersim then function self:OnSave()
    return
    {
        eruption = _eruption:value(),
        eruption_timer = _eruption_timer:value(),
        firerain_duration = _firerain_duration:value(),
        ashrain_duration = _ashrain_duration:value(),
        smokecloud_duration = _smokecloud_duration:value(),

        firerain_delay = _firerain_delay:value(),
        ashrain_delay = _ashrain_delay:value(),
        smokecloud_delay = _smokecloud_delay:value(),

        firerain_spawn_per_sec = _firerain_spawn_per_sec,
        quaketime = _warntask ~= nil and GetTaskRemaining(_warntask) or nil,

        eruptstinger = _eruptquakesound:value()
    }
end end

if _ismastersim then function self:OnLoad(data)
    if data ~= nil then
        if data.eruption then
            if data.firerain_duration then
                StartFireRain(data.firerain_duration, data.firerain_delay, data.firerain_spawn_per_sec)
            end
            if data.ashrain_duration then
                StartAshRain(data.ashrain_duration, data.ashrain_delay)
            end
            if data.smokecloud_duration then
                StartSmokeCloud(data.smokecloud_duration, data.smokecloud_delay)
            end
            if data.quaketime then
                StartWarnSound(data.quaketime)
            end
            if data.eruption_timer then
                _eruption_timer:set(data.eruption_timer)
            end
            if data.eruptstinger then
                StartEruptSound()
            end
            
            print("resuming eruption")
            _eruption:set(true)
        end
    end
end end

--------------------------------------------------------------------------
--[[ Debug ]]
--------------------------------------------------------------------------

-- function self:GetDebugString()
--     return string.format("  next quake %d, next eruption %d\n  firerain timer %4.2f, int %4.2f, rate %4.2f, %4.2f/s\n  ash timer %4.2f, dur %4.2f, %d particles/tick\n  smoke timer %4.2f, dur %4.2f, rate %4.2f\n  appease segs %4.2f\n",
--         self:GetNumSegmentsUntilQuake() or 0, self:GetNumSegmentsUntilEruption() or 0,
--         _firerain_timer:value(), _firerain_duration:value(), _firerain_spawn_rate, _firerain_spawn_per_sec,
--         _ashrain_timer:value(), _ashrain_duration:value(), (_ashrainfx and _ashrainfx.particles_per_tick) or 0,
--         _smoke_timer:value(), _smokecloud_duration:value(), GetSmokeCloudRate(),
--         _appeasesegs)
-- end
    
--------------------------------------------------------------------------
--[[ End ]]
--------------------------------------------------------------------------

end)
        