--------------------------------------------------------------------------
--[[ VolcanoManager class definition ]]
--------------------------------------------------------------------------

return Class(function(self, inst)

--------------------------------------------------------------------------
--[[ Dependencies ]]
--------------------------------------------------------------------------

local math = math
local SCHEDULE_DEFS = require("prefabs/volcanoschedule_defs")

--------------------------------------------------------------------------
--[[ Constants ]]
--------------------------------------------------------------------------

local SEG_TIME = TUNING.SEG_TIME

--------------------------------------------------------------------------
--[[ Member variables ]]
--------------------------------------------------------------------------

--Public
self.inst = inst

--Private
local _world = TheWorld
local _isenabled = true

--Master simulation
local _volcano
local _schedule
local _schedulesegs
local _staffschedule
local _staffschedulesegs
local _staffstartseg
local _highest_schedule_seg
local _appeasesegs

local _seasonprogress = 0
local _isdryseason = false
local _time = 0
local _cycles = 0
local _drylength = 0

--------------------------------------------------------------------------
--[[ Schedule functions ]]
--------------------------------------------------------------------------

local function segs_until_event(event, schedule, currentSeg)
    if schedule then
        local earliest = nil
        local ev = nil

        for k, v in pairs(schedule) do
            for i = 1, #v, 1 do
                if v[i].event == event then
                    local seg = k
                    if earliest == nil or (seg > currentSeg and seg < earliest) then
                        earliest = seg
                        ev = v[i]
                    end
                end
            end
        end
        if earliest then
            return earliest - currentSeg, ev
        end
    end
    return nil, nil
end

local function RunSchedule(schedule, segs)
    if schedule then
        for seg, _ in pairs(schedule) do
            if seg <= segs then
                for i = 1, #schedule[seg], 1 do
                    _world:PushEvent(schedule[seg][i].event, schedule[seg][i].data)
                end
                schedule[seg] = nil
            end
        end
    end
    _highest_schedule_seg = math.max(_highest_schedule_seg, segs)
end


local function BuildSchedule(scheduledef, startseg)
    local newschedule = {}

    startseg = startseg or 0

    local lasterupt = 0
    local segs = 0
    for i = 1, #scheduledef, 1 do
        local erupt = scheduledef[i]
        segs = segs + 16 * (erupt.days or 0) + (erupt.segs or 0)
        if segs > startseg then
            if erupt.warnquake then
                for j = 1, #erupt.warnquake, 1 do
                    local quakesegs = segs - erupt.warnquake[j].segsprev
                    if quakesegs >= 0 and quakesegs >= startseg then
                        newschedule[quakesegs] = newschedule[quakesegs] or {}
                        table.insert(newschedule[quakesegs], { event = "ms_warnquake", data = erupt.warnquake[j].size })
                    end
                end
            end

            newschedule[segs] = newschedule[segs] or {}
            
            local data = {
                firerain_duration = (erupt.data.firerain_duration or 1) * SEG_TIME,
                ashrain_duration = (erupt.data.ashrain_duration or 1) * SEG_TIME,
                smokecloud_duration = (erupt.data.smokecloud_duration or 1) * SEG_TIME,

                firerain_delay = (erupt.data.firerain_delay or 0) * SEG_TIME,
                ashrain_delay = (erupt.data.ashrain_delay or 0) * SEG_TIME,
                smokecloud_delay = (erupt.data.smokecloud_delay or 0) * SEG_TIME,

                firerain_per_sec = erupt.data.firerain_per_sec or 0.125,
                total_segs = segs - lasterupt,
                num = erupt.data.num or 1,
            }
            lasterupt = segs + (erupt.data.firerain_delay or 0) + (erupt.data.firerain_duration or 1)

            table.insert(newschedule[segs], {event = "ms_starteruption", data = data})
        else
            erupt.data = erupt.data or {}
            lasterupt = segs + (erupt.data.firerain_delay or 0) + (erupt.data.firerain_duration or 1)
        end
    end

    return newschedule, segs
end

local function RebuildSchedule()
    if _isenabled then
        local volcanoschedule = deepcopy(SCHEDULE_DEFS.DRY_SEASON)
        _schedule, _schedulesegs = BuildSchedule(volcanoschedule, _highest_schedule_seg)
        inst:StartUpdatingComponent(self)
    end
end

local function RebuildWarningSchedule(schedule, segs)
    if schedule then
        local dry, dryev = segs_until_event("ms_starteruption", schedule, segs)
        if dry and dryev then
            local eruption_seg = dry + segs
            for _, v in ipairs(SCHEDULE_DEFS.DRY_SEASON[dryev.data.num].warnquake) do
                local quake_seg = eruption_seg - v.segsprev
                if quake_seg > segs and not schedule[quake_seg] then
                    schedule[quake_seg] = {}
                    table.insert(schedule[quake_seg], { event = "ms_warnquake", data = v.size })
                end
            end
        end
    end
end

local function ExtendDrySchedule(schedule, start_segs)
    if schedule then
        local erupt = SCHEDULE_DEFS.DRY_SEASON[17]
        local segs = (start_segs or 0) + 16 * (erupt.days or 0) + (erupt.segs or 0)
        local data = {
            firerain_duration = (erupt.data.firerain_duration or 1) * SEG_TIME,
            ashrain_duration = (erupt.data.ashrain_duration or 1) * SEG_TIME,
            smokecloud_duration = (erupt.data.smokecloud_duration or 1) * SEG_TIME,

            firerain_delay = (erupt.data.firerain_delay or 0) * SEG_TIME,
            ashrain_delay = (erupt.data.ashrain_delay or 0) * SEG_TIME,
            smokecloud_delay = (erupt.data.smokecloud_delay or 0) * SEG_TIME,

            firerain_per_sec = erupt.data.firerain_per_sec or 0.125,
            total_segs = 16 * (erupt.days or 0) + (erupt.segs or 0),
            num = erupt.data.num or 1,
        }

        schedule[segs] = {}
        table.insert(schedule[segs], {event = "ms_starteruption", data = data})
        _schedulesegs = segs
    end
end

local function RebuildStaffSchedule(startsegs)
    if _isenabled then
        local volcanoschedule = deepcopy(SCHEDULE_DEFS.STAFF_TRAP)
        _staffstartseg = startsegs
        _staffschedule, _staffschedulesegs = BuildSchedule(volcanoschedule, self:GetCurrentStaffScheduleSegment())
        inst:StartUpdatingComponent(self)
    end
end

--------------------------------------------------------------------------
--[[ Private member functions ]]
--------------------------------------------------------------------------

local function StartDry()
    print("VolcanoManager starting dry season")
    _isdryseason = true
    
    _highest_schedule_seg = 0
    RebuildSchedule()
end

local function StopDry()
    print("VolcanoManager stopping dry season")
    _isdryseason = false

    _appeasesegs = 0
end

local function UpdateDrySeason(isdry)
    if _isdryseason ~= isdry then
        if isdry then
            StartDry()
        else
            StopDry()
        end
    end
end

--------------------------------------------------------------------------
--[[ Event Callbacks ]]
--------------------------------------------------------------------------

local function OnSeasonTick(src, data)
    _seasonprogress = data.progress
    UpdateDrySeason(data.season == "summer")
end

local function OnClockTick(src, data)
    _time = data.time
end

local function OnCyclesChanged(src, cycles)
    _cycles = cycles
end

local function OnSeasonLengthsChanged(src, data)
    _drylength = data.summer
end

local function DoStaffTrap()
    print("VolcanoManager start staff trap")
    RebuildStaffSchedule( math.floor( 16 * (_cycles + _time) ) )
end

local function OnStartStaffTrap()
    for id in pairs(Shard_GetConnectedShards()) do
        SendModRPCToShard(SHARD_MOD_RPC["Island Adventure"]["StartStaffTrap"], id)
    end
    DoStaffTrap()
end

local function DoAppeasement(src, segs)
    if segs < 0 then --Schedule is getting pushed forward from a wrath item

        local segsuntilquake = self:GetNumSegmentsUntilQuake()
        local segsuntilerupt = self:GetNumSegmentsUntilEruption()

        local numSegs = math.abs(segs)
        local doquake = segsuntilquake and segsuntilquake < numSegs
        local doerupt = segsuntilerupt and segsuntilerupt < numSegs

        if doquake then
            print("Wrath causing warn quake")
            _world:PushEvent("ms_warnquake")
            if doerupt then
                print("and eruption")
                _world:DoTaskInTime(SCHEDULE_DEFS.WARN_QUAKE.med.duration + 1.0 , function()
                    local erupt = self:GetNextEruptionEvent()
                    _world:PushEvent("ms_starteruption", erupt.data)
                end)
            end
        elseif doerupt then
            print("Wrath causing eruption")
            _world:PushEvent("ms_starteruption")
        end

    end
    _appeasesegs = _appeasesegs + segs

    RebuildSchedule()
end

local function OnAppeaseVolcano(src, segs)
    for id in pairs(Shard_GetConnectedShards()) do
        SendModRPCToShard(SHARD_MOD_RPC["Island Adventure"]["AppeaseVolcano"], id, segs)
    end
    DoAppeasement(src, segs)
end

--------------------------------------------------------------------------
--[[ Public member functions ]]
--------------------------------------------------------------------------

function self:GetCurrentStaffScheduleSegment()
    return math.floor( 16 * (_cycles + _time) ) - _staffstartseg
end

function self:GetCurrentScheduleSegment()
    local cycles = _seasonprogress * _drylength
    return math.floor(16 * (cycles + _time)) - _appeasesegs
end

function self:GetNumSegmentsUntilEvent(event)
    local dry, dryev = segs_until_event(event, _schedule, self:GetCurrentScheduleSegment())
    local staff, staffev = segs_until_event(event, _staffschedule, self:GetCurrentStaffScheduleSegment())
    if not dry and not staff then
        return nil
    end
    return math.min(dry or math.huge, staff or math.huge)
end

function self:GetNumSegmentsUntilEruption()
    return self:GetNumSegmentsUntilEvent("ms_starteruption")
end

function self:GetNumSegmentsUntilQuake()
    return self:GetNumSegmentsUntilEvent("ms_warnquake")
end

function self:GetNumSegmentsOfEruption()
    local dry, dryev = segs_until_event("ms_starteruption", _schedule, self:GetCurrentScheduleSegment())
    if dry and dryev and dryev.data and dryev.data.total_segs then
        return dryev.data.total_segs
    end
    return 66
end

function self:GetNextEruptionEvent()
    local ev = nil
    local dry, dryev = segs_until_event("ms_starteruption", _schedule, self:GetCurrentScheduleSegment())
    local staff, staffev = segs_until_event("ms_starteruption", _staffschedule, self:GetCurrentStaffScheduleSegment())
    if dryev and staffev then
        if dry < staff then
            ev = dryev
        else
            ev = staffev
        end
    elseif dryev then
        ev = dryev
    elseif staffev then
        ev = staffev
    end
    return ev
end

local function OnVolcanoRemoved()
    _volcano = nil
end

function self:SetVolcano(volcano)
	if _volcano ~= volcano then
        if _volcano then
            _volcano:RemoveEventCallback("onremove", OnVolcanoRemoved)
            _volcano = nil
        end

        if volcano then
            volcano:ListenForEvent("onremove", OnVolcanoRemoved)
            _volcano = volcano
        end
    end
end

function self:GetVolcano()
    return _volcano
end

function self:SetEnabled(enable)
    _isenabled = enable
end

--------------------------------------------------------------------------
--[[ Update ]]
--------------------------------------------------------------------------

function self:OnUpdate(dt)
    if not _isenabled or not _isdryseason and _staffschedule == nil then
        inst:StopUpdatingComponent(self)
        return
    end
    --update schedule
    if _schedule then
        local segs = self:GetCurrentScheduleSegment()
        RunSchedule(_schedule, segs)
        if segs > _schedulesegs then
            ExtendDrySchedule(_schedule, segs)
            RebuildWarningSchedule(_schedule, segs) -- TODO: idk about this...
        end
    end
    --update staff schedule
    if _staffschedule then
        local segs = self:GetCurrentStaffScheduleSegment()
        RunSchedule(_staffschedule, segs)
        if segs > _staffschedulesegs then
            _staffschedule = nil
        end
    end
end

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------

--Register events
inst:ListenForEvent("seasontick", OnSeasonTick, _world)
inst:ListenForEvent("clocktick", OnClockTick, _world)
inst:ListenForEvent("cycleschanged", OnCyclesChanged, _world)
inst:ListenForEvent("seasonlengthschanged", OnSeasonLengthsChanged, _world)

inst:ListenForEvent("ms_startstafftrap", OnStartStaffTrap, _world)
inst:ListenForEvent("ms_appeasevolcano", OnAppeaseVolcano, _world)

inst:ListenForEvent("shard_startstafftrap", DoStaffTrap, _world)
inst:ListenForEvent("shard_appeasevolcano", DoAppeasement, _world)

--Initialize variables
_volcano = nil
_schedule = {}
_schedulesegs = 0
_staffschedule = {}
_staffschedulesegs = 0
_staffstartseg = 0
_highest_schedule_seg = 0
_appeasesegs = 0

--------------------------------------------------------------------------
--[[ Save/Load ]]
--------------------------------------------------------------------------

function self:OnSave()
    return
    {
        appeasesegs = _appeasesegs,
        staffstartseg = _staffstartseg,
        highest_schedule_seg = _highest_schedule_seg,

        dry = _schedule ~= nil,
        staff = _staffschedule ~= nil
    }
end

function self:OnLoad(data)
    if data ~= nil then
        _appeasesegs = data.appeasesegs or _appeasesegs
        _staffstartseg = data.staffstartseg or _staffstartseg
        _highest_schedule_seg = data.highest_schedule_seg or self:GetCurrentScheduleSegment()

        if data.dry then
            print("rebuilding schedule...")
            RebuildSchedule()
        end
        if data.staff then
            print("resuming staff schedule...", _staffstartseg)
            RebuildStaffSchedule(_staffstartseg)
        end
    end
end

--------------------------------------------------------------------------
--[[ Debug ]]
--------------------------------------------------------------------------

function self:GetDebugString()
    local cycles = _cycles
    local normtime = _time
    local cursegs = math.floor(16 * (cycles + normtime))
    local str = "Volcano\n"

    if _schedule then
        str = str .. string.format("  dry segs %d, season %4.2f\n", self:GetCurrentScheduleSegment(), _seasonprogress)
    end
    if _staffschedule then
        str = str .. string.format("  staff segs %d, start seg %d\n", cursegs - _staffstartseg, _staffstartseg)
    end

    str = str .. string.format("  next quake %d, next eruption %d\n  cycles %d, normtime %f, cursegs %d\n  appease segs %4.2f\n",
        self:GetNumSegmentsUntilQuake() or 0, self:GetNumSegmentsUntilEruption() or 0,
        cycles, normtime, cursegs, _appeasesegs)

    return str
end
    
--------------------------------------------------------------------------
--[[ End ]]
--------------------------------------------------------------------------

end)
    