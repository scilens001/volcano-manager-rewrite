require("stategraphs/commonstates")

local events = {

}

local states=
{

    State{
		name = "active_pre",

		tags = {"idle", "active"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("active_idle_pre")
            inst:SetActive(true)
		end,
		
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("active") end)
		},
	},

	State{
		name = "active",

		tags = {"idle", "active"},

		onenter = function(inst)
			inst.AnimState:PushAnimation("active_idle")
            inst:SetActive(true)
		end,
		
		events =
		{
			EventHandler("animover", function(inst) 
                if TheWorld.state.issummer then
                    inst.sg:GoToState("active")
                else
                    inst.sg:GoToState("active_pst")
                end
            end)
		},
	},

	State{
		name = "active_pst",
		
		tags = {"idle", "active"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("active_idle_pst")
            inst:SetActive(true)
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("dormant_pre") end)
		},
	},

    State{
		name = "dormant_pre",

		tags = {"idle"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("dormant_idle_pre")
            inst:SetActive(false)
		end,
		
		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("dormant") end)
		},
	},

	State{
		name = "dormant",

		tags = {"idle"},

		onenter = function(inst)
			inst.AnimState:PushAnimation("dormant_idle")
            inst:SetActive(false)
		end,
		
		events =
		{
			EventHandler("animover", function(inst)
                if TheWorld.state.issummer then
                    inst.sg:GoToState("dormant_pst")
                else
                    inst.sg:GoToState("dormant")
                end
            end)
		},
	},

	State{
		name = "dormant_pst",
		
		tags = {"idle"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("dormant_idle_pst")
            inst:SetActive(false)
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("active_pre") end)
		},
	},

	State{
		name = "erupt",

		tags = {"busy", "active"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("rumble")
            inst:SetActive(true)
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("erupt_loop") end)
		},
	},

	State{
		name = "erupt_loop",

		tags = {"busy", "active"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("erupt")
            inst:SetActive(true)
		end,
		
		timeline =
        {
            TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound("ia/common/volcano/volcano_erupt_charge") end),
            TimeEvent(63*FRAMES, function(inst) inst.SoundEmitter:PlaySound("ia/common/volcano/volcano_erupt") end),
            TimeEvent(72*FRAMES, function(inst) inst.SoundEmitter:PlaySound("ia/common/volcano/volcano_erupt_sizzle") end),
        },

        events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("erupt_loop") end)
		},
	},

	State{
		name = "rumble",
		
		tags = {"idle", "active"},

		onenter = function(inst)
			inst.AnimState:PlayAnimation("rumble")
            inst:SetActive(true)
		end,

		events =
		{
			EventHandler("animover", function(inst) inst.sg:GoToState("active") end)
		},
	},
}

return StateGraph("volcano", states, events, "dormant")
