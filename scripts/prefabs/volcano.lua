local assets =
{
	Asset("ANIM", "anim/volcano.zip"),
}

local function SetDefaultState(inst)
    local _world = TheWorld
    if _world.state.israiningfire then
        inst.sg:GoToState("erupt_loop")
    elseif _world.state.issummer then
        inst.sg:GoToState("active")
    else
        inst.sg:GoToState("dormant")
    end
end

local function OnWake(inst)
    inst.SoundEmitter:PlaySound("ia/common/volcano/volcano_external_amb", "volcano")
    inst:SetActive(inst.active, true)
    SetDefaultState(inst)
end

local function OnSleep(inst)
    inst.SoundEmitter:KillSound("volcano")
    SetDefaultState(inst)
end

-- local maxmod = 70
-- local distToFinish = 10 * 10 --Distance to volcano where you reach max zoom
-- local distToStart = 65 * 65 --Distance from the volcano where you start to zoom

local function CalcCameraDistMod(camera, mod, data)
	local dist = data.inst:GetDistanceSqToPoint(camera.currentpos)
	-- if dist < distToStart then --is in range
	if dist < 4225 then
		mod = mod +
			-- (  dist < distToFinish and maxmod --peak
			(  dist < 100 and 70
			-- or maxmod * (1 - (dist - distToFinish) / (distToStart - distToFinish))  ) --Lerp
			or 70 * (1 - (dist - 100) / 4125)  )
	end
	return mod
end

local _SetDestinationWorld = nil
local function SetDestinationWorld(self, world, permanent, ...)
    world = IA_CONFIG.volcanoid or world  --Guaranteed in multiplayer world to teleport players to a volcano world  -Jerry
    return _SetDestinationWorld(self, world, permanent, ...)
end

local function OnLoadPostPass(inst)
    if TheWorld.components.volcanomanager then
        TheWorld.components.volcanomanager:SetVolcano(inst)
    end
end

local function OnRemoveEntity_camera(inst)
    if TheCamera and TheCamera.envdistancemods then
		for k, v in pairs(TheCamera.envdistancemods) do
			if v.inst == inst then
				table.remove(TheCamera.envdistancemods, k)
				return
			end
		end
	end
end

local function OnIsRainingFireChanged(inst, israining)
    if inst:IsAsleep() then
        SetDefaultState(inst)
    elseif israining then
        inst.sg:GoToState("erupt")
    else
        inst.sg:GoToState("rumble")
    end
end

local function OnIsVolcanoActiveChanged(inst)
    if inst:IsAsleep() then
        SetDefaultState(inst)
    end
end

local function OnVolcanoWarningQuake(inst)
    if inst:IsAsleep() then
        SetDefaultState(inst)
    elseif not inst.sg:HasStateTag("busy") then
        inst.sg:GoToState("rumble")
    end
end

local function SetActive(inst, active, forceupdate)
    if not forceupdate and inst.active == active then return end
    if active then
        inst.icon.MiniMapEntity:SetIcon("volcano_active.tex")
        inst.SoundEmitter:SetParameter("volcano", "volcano_state", 1.0)
    else
        inst.icon.MiniMapEntity:SetIcon("volcano.tex")
        inst.SoundEmitter:SetParameter("volcano", "volcano_state", 0.0)
    end
    inst.active = active
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

    inst.AnimState:SetBuild("volcano")
    inst.AnimState:SetBank("volcano")
    inst.AnimState:PlayAnimation("dormant_idle", true)

    inst.entity:AddLight()
    inst.Light:SetFalloff(0.4)
    inst.Light:SetIntensity(.7)
    inst.Light:SetRadius(10)
    inst.Light:SetColour(249/255, 130/255, 117/255)
    inst.Light:Enable(true)

    inst:AddTag("TheVolcano")
    inst:AddTag("ignorewalkableplatforms")

    MakeWaterObstaclePhysics(inst, 10, 5, 1)

    inst.entity:SetPristine()

	if not TheNet:IsDedicated() then
        -- TODO: InteriorCamera support
		if TheCamera and TheCamera.envdistancemods then
			table.insert(TheCamera.envdistancemods, {fn = CalcCameraDistMod, inst = inst})
		else
			print(inst,"PANIC! no camera!")
		end
        inst.OnRemoveEntity = OnRemoveEntity_camera
	end

    if not TheWorld.ismastersim then
        return inst
    end

    ------------------------------------------------
    inst.icon = SpawnPrefab("globalmapiconunderfog")
    inst:DoTaskInTime(0, function() inst.icon:TrackEntity(inst) inst:SetActive(inst.active, true) end)
    ------------------------------------------------

    inst:AddComponent("inspectable")

    inst:AddComponent("worldmigrator")
	inst.components.worldmigrator.id = 999
	inst.components.worldmigrator.receivedPortal = 999

    inst:AddComponent("migratorboatstorage") 

    if not _SetDestinationWorld then
        _SetDestinationWorld = inst.components.worldmigrator.SetDestinationWorld
    end
    inst.components.worldmigrator.SetDestinationWorld = SetDestinationWorld

    inst.OnLoadPostPass = OnLoadPostPass

    inst:WatchWorldState("israiningfire", OnIsRainingFireChanged)
    inst:WatchWorldState("issummer", OnIsVolcanoActiveChanged)

    inst:ListenForEvent("OnVolcanoWarningQuake", function()
        inst:PushEvent("startrumble")
        OnVolcanoWarningQuake(inst)
    end, TheWorld)

    inst.SetActive = SetActive

    inst:SetStateGraph("SGvolcano")

    SetDefaultState(inst)

    inst.OnEntityWake = OnWake
    inst.OnEntitySleep = OnSleep

	return inst
end

return Prefab("volcano", fn, assets)
