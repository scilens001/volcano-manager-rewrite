local erupt_assets =
{
	Asset("ANIM", "anim/lava_erupt.zip"),
}

local bubble_assets =
{
	Asset("ANIM", "anim/lava_erupt.zip"),
	Asset("ANIM", "anim/lava_bubbling.zip"),
}

local prefabs = {
    "lava_erupt",
    "lava_bubbling"
}

local function GetPrefab()
    if TheWorld.state.israiningfire and math.random() < 0.5 then
        return "volcanolavafx_erupt"
    end
    return "volcanolavafx_bubbling"
end

local function CanSpawn(inst, ground, x, y, z)
    return IsSurroundedByTile(x, y, z, 6, WORLD_TILES.VOLCANO_LAVA)
end

local function SetRadius(inst, radius)
    inst._radius:set(radius)
    inst.Light:SetRadius(inst._radius:value())
end

local function OnRadiusDirty(inst)
    inst.components.areaspawner:SetDensityInRange(inst._radius:value())
end

local function OnEntitySleep(inst)
    inst.components.areaspawner:Stop()
end

local function OnEntityWake(inst)
    inst.components.areaspawner:Start()
end

local function spawnerfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddLight()
    inst.entity:AddNetwork()

    inst.Light:SetIntensity(0.2)
    inst.Light:SetFalloff(2.5)
    inst.Light:SetColour(255/255,84/255,61/255)

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst._radius = net_float(inst.GUID, "volcanolavafx._radius", "radiusdirty")
    
    inst.entity:SetPristine()

    if not TheNet:IsDedicated() then
        inst:AddComponent("areaspawner")
        inst.components.areaspawner:SetPrefabFn(GetPrefab)
        inst.components.areaspawner:SetSpawnTestFn(CanSpawn)
        inst.components.areaspawner:SetRandomTimes(0.5, 0.25)
        inst.components.areaspawner:SetValidTileType(WORLD_TILES.VOLCANO_LAVA)

        inst:ListenForEvent("radiusdirty", OnRadiusDirty)

        inst.OnEntitySleep = OnEntitySleep
        inst.OnEntityWake = OnEntityWake
	end

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false
    inst.SetRadius = SetRadius

    return inst
end

local function commonfn(Sim)
	local inst = CreateEntity()
    
    inst:AddTag("CLASSIFIED")
    --[[Non-networked entity]]
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()

	inst:AddTag("FX")

	inst.persists = false
	inst:ListenForEvent("animover", inst.Remove)
    inst:ListenForEvent("entitysleep", inst.Remove)

	return inst	
end

local function eruptfn(Sim)
	local inst = commonfn(Sim)
	inst.AnimState:SetBank("lava_erupt")
	inst.AnimState:SetBuild("lava_erupt")
	inst.AnimState:PlayAnimation("idle")
	inst.SoundEmitter:PlaySound("dontstarve_DLC002/volcano_amb/volcano_rock_launch")
	return inst
end

local function bubblefn(Sim)
	local inst = commonfn(Sim)
	inst.AnimState:SetBank("lava_bubbling")
	inst.AnimState:SetBuild("lava_erupt")
	inst.AnimState:PlayAnimation("idle")
	inst.SoundEmitter:PlaySound("dontstarve_DLC002/volcano_amb/lava_bubbling")
	return inst
end

return Prefab("volcanolavafx", spawnerfn, nil, prefabs), 
    Prefab("volcanolavafx_erupt", eruptfn, erupt_assets),
    Prefab("volcanolavafx_bubbling", bubblefn, bubble_assets)
