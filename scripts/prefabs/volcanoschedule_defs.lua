local ERUPT_QUAKE = {
    ["small"] = {duration = 4.0/2, speed = 0.02, scale = 2.0/2},
    ["med"] = {duration = 4.0, speed = 0.02, scale = 2.0},
    ["large"] = {duration = 2*4.0, speed = 0.02, scale = 2*2.0},
}

local WARN_QUAKE = {
    ["small"] = {duration = 0.7/2, speed = 0.02, scale = 0.75/2},
    ["med"] = {duration = 0.7, speed = 0.02, scale = 0.75},
    ["large"] = {duration = 2*0.7, speed = 0.02, scale = 2*0.75},
}

--The schedule used in dry season
local DRY_SEASON =
{
	{-------------------------------------------erupt 1
		days = 3, --day61
		segs = 3,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 1,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 5,
			smokecloud_delay = 1,
			smokecloud_duration = 4,
			num = 1,
		},
		warnquake =
		{
			{segsprev = 16, size="small"},
			{segsprev = 8, size="med"},
			{segsprev = 4, size="large"}
		}
	},
	{------------------------------------------------------erupt 2
		days = 2, --day63
		segs = 6,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 1,
			firerain_per_sec = .5, 
			ashrain_delay = 1,
			ashrain_duration = 5,
			smokecloud_delay = 1,
			smokecloud_duration = 4,
			num = 2,
		},
		warnquake =
		{
			{segsprev = 16, size="small"},
			{segsprev = 8, size="med"},
			{segsprev = 4, size="large"}
		}
	},
	{--------------------------------------------erupt3

		days = 1, ---day65
		segs = 10,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 2,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 6,
			smokecloud_delay = 1,
			smokecloud_duration = 5,
			num = 3,
		},
		warnquake =
		{
			{segsprev = 16, size="small"},
			{segsprev = 8, size="med"},
			{segsprev = 4, size="large"}
		}
	},
	{----------------------------------------erupt 4
		days = 1,  --day66
		segs = 5,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 2,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 7,
			smokecloud_delay = 1,
			smokecloud_duration = 6,
			num = 4,
		},
		warnquake =
		{
			{segsprev = 16, size="small"},
			{segsprev = 8, size="med"},
			{segsprev = 4, size="large"}
		}
	},
	{-----------------------------------------------erupt 5
		days = 1, --day 67
		segs = 8,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,--5 seconds
			ashrain_delay = 1,
			ashrain_duration = 8,
			smokecloud_delay = 1,
			smokecloud_duration = 7,
			num = 5,
		},
		warnquake =
		{
			{segsprev = 16, size="small"},
			{segsprev = 8, size="med"},
			{segsprev = 4, size="large"}
		}
	},
	{-------------------------------------------------------erupt6
		days = 1, --day 68
		segs = 4,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,-- 4 seconds
			ashrain_delay = 1,
			ashrain_duration = 8,
			smokecloud_delay = 1,
			smokecloud_duration = 7,
			num = 6,
		},
		warnquake =
		{
			{segsprev = 16, size="small"},
			{segsprev = 8, size="med"},
			{segsprev = 4, size="large"}
		}
	},
	{-----------------------------------------------erupt7
		days = 1, --day69
		segs = 8,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 7,
		},
		warnquake =
		{
			{segsprev = 16, size="small"},
			{segsprev = 8, size="med"},
			{segsprev = 4, size="large"}
		}
	},
	{---------------------------------------------erupt8
		days = 1, --day69
		segs = 4,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 8,
		},
		warnquake =
		{
			{segsprev = 12, size="small"},
			{segsprev = 6, size="med"},
			{segsprev = 2, size="large"}
		}
	},
	{----------------------------------------ERUPT 9
		days = 1,
		segs = 2,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 8,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},
	{-----------------------------------erupt 10
		days = 0,
		segs = 14,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 10,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},
	{------------------------------------------erupt 11
		days = 0,
		segs = 11,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 11,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},
	{---------------------------------------------erupt 12
		days = 0,
		segs = 9,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 12,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},
	{-------------------------------------------erupt 13
		days = 0,
		segs = 8,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,--
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 13,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},
	{-----------------------------------------erupt 14
		days = 0,
		segs = 8,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 14,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},
	{---------------------------------------erupt15
		days = 0,
		segs = 8,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = 1,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 15,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},
	{---------------------------------------erupt16
		days = 0,
		segs = 8,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = 1,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 16,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},

	{---------------------------------------erupt17
		days = 0,
		segs = 8,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 3,
			firerain_per_sec = 1,
			ashrain_delay = 1,
			ashrain_duration = 9,
			smokecloud_delay = 1,
			smokecloud_duration = 8,
			num = 17,
		},
		warnquake =
		{
			{segsprev = 4, size="small"},
			{segsprev = 2, size="med"},
			{segsprev = 1, size="large"}
		}
	},

}

--The schedule used for the Volcano Staff trap
local STAFF_TRAP =
{
	{
		days = 0,
		segs = 1,
		data =
		{
			firerain_delay = 0,
			firerain_duration = 1,
			firerain_per_sec = .5,
			ashrain_delay = 1,
			ashrain_duration = 5,
			smokecloud_delay = 1,
			smokecloud_duration = 4,
		},
		warnquake =
		{
			{segsprev = 1, size="large"}
		}
	},
}
return {
    ERUPT_QUAKE = ERUPT_QUAKE,
    WARN_QUAKE = WARN_QUAKE,
    DRY_SEASON = DRY_SEASON, 
    STAFF_TRAP = STAFF_TRAP,
}