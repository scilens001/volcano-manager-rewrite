local STRINGS = GLOBAL.STRINGS

AddStartLocation("ShipwreckedStart", {
    name = STRINGS.UI.SANDBOXMENU.SHIPWRECKED,
    location = "forest",
    start_setpeice = "ShipwreckedStart",
    start_node = "BeachSandHome",  -- "BeachSandHome_Spawn"
})