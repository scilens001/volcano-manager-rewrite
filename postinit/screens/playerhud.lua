local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local ContainerWidget = require("widgets/containerwidget")
local PoisonOver = require("widgets/poisonover")
local BoatOver = require("widgets/boatover")
local UIAnim = require("widgets/uianim")
local PlayerHud = require("screens/playerhud")

local __ctor = PlayerHud._ctor
function PlayerHud:_ctor(...)
    __ctor(self, ...)
    self.inst:WatchWorldState("smokecloud_density", function(inst, den) self:UpdateSmokeCloud(den) end)
end

local _CreateOverlays = PlayerHud.CreateOverlays
function PlayerHud:CreateOverlays(owner, ...)
    _CreateOverlays(self, owner, ...)

    self.poisonover = self.overlayroot:AddChild(PoisonOver(owner))
    self.boatover = self.overlayroot:AddChild(BoatOver(owner))

    self.smoke = self.overlayroot:AddChild(UIAnim())
    self.smoke:SetClickable(false)
    self.smoke:SetHAnchor(ANCHOR_MIDDLE)
    self.smoke:SetVAnchor(ANCHOR_MIDDLE)
    self.smoke:GetAnimState():SetBank("clouds_ol")
    self.smoke:GetAnimState():SetBuild("clouds_ol")
    self.smoke:GetAnimState():PlayAnimation("idle", true)
    self.smoke:GetAnimState():SetMultColour(1, 1, 1, 0)
    self.smoke:SetScaleMode(SCALEMODE_FIXEDSCREEN_NONDYNAMIC) -- Because klei is never going to fix this on clouds...
    self.smoke:Hide()
end

function PlayerHud:GetOpenContainerWidgets()
    return self.controls.containers
end

function PlayerHud:OpenBoat(boat, sailing)
    if boat then
        local boatwidget = nil
        if sailing then
            self.controls.inv.boatwidget = self.controls.inv.root:AddChild(ContainerWidget(self.owner))
            boatwidget = self.controls.inv.boatwidget
            boatwidget:SetScale(1)
            boatwidget.scalewithinventory = false
            boatwidget:MoveToBack()
            boatwidget.inv_boatwidget = true
            self.controls.inv:Rebuild()
        else
            boatwidget = self.controls.containerroot:AddChild(ContainerWidget(self.owner))
        end

        boatwidget:Open(boat, self.owner, not sailing)

        for k,v in pairs(self.controls.containers) do
            if v.container then
                if v.parent == boatwidget.parent or k == boat then
                    v:Close()
                end
            else
                self.controls.containers[k] = nil
            end
        end

        self.controls.containers[boat] = boatwidget
    end
end

function PlayerHud:UpdateSmokeCloud(den)
    print("UPDATE SMOKECLOUD", den)
    if den > 0.0 then
        local g = 0.5
        local a = math.sin(PI * den)
        self.smoke:Show()
        self.smoke:GetAnimState():SetMultColour(g, g, g, a)
    else
        self.smoke:Hide()
    end
end