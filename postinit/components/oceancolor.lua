local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local OceanColor = require("components/oceancolor")


local _Initialize = OceanColor.Initialize
function OceanColor:Initialize(has_ocean, ...)
    if TheWorld.has_ia_ocean then
        return _Initialize(self, false, ...)
    end
    return _Initialize(self, has_ocean, ...)
end