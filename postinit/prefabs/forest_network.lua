local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddPrefabPostInit("forest_network", function(inst)

    local _world = TheWorld
    if _world:HasTag("island") or _world:HasTag("volcano") then
        inst:AddComponent("volcanoactivity")

        if inst.components.weather then
            inst.components.weather.cannotsnow = true
        end
    end
end)